//
//  CategoryCVC.swift
//  Salt&Pepper
//
//  Created by Rudy E Matos on 6/14/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class CategoryCVC: UICollectionViewCell {
    
    @IBOutlet weak var categoryNameLBL: UILabel!
    
    func configureView(text : String){
        categoryNameLBL.text = text
    }
    
    
}
