//
//  OptionItemCell.swift
//  Salt&Pepper
//
//  Created by Rudy E Matos on 6/26/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class OptionItemTVC: UITableViewCell {

    @IBOutlet weak var selectedIV: UIImageView!
    @IBOutlet weak var optionItemName: UILabel!
    @IBOutlet weak var optionItemDescription: UILabel!
    @IBOutlet weak var optionItemPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configureView(productOption : ProductOption.Option){
        
        optionItemName.text = productOption.name
        optionItemDescription.text = productOption.description
        optionItemPrice.text = "$\(Int(productOption.price))"
        
        if productOption.isSelected{
            selectedIV.image = UIImage(named: "selected")
            optionItemName.alpha = 1
            optionItemDescription.alpha = 1
            optionItemPrice.alpha = 1
        }else{
            selectedIV.image = UIImage(named: "empty_selection")
            optionItemName.alpha = 0.5
            optionItemDescription.alpha = 0.5
            optionItemPrice.alpha = 0.5
        }
        
    }
    
}
