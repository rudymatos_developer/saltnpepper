//
//  ConfirmationProductOptionTVC.swift
//  Salt&Pepper
//
//  Created by Rudy E Matos on 6/29/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class ConfirmationProductOptionTVC: UITableViewCell {
    @IBOutlet weak var productOptionNameLBL: UILabel!
    @IBOutlet weak var productOptionPriceLBL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureView(option: ProductOption.Option){
        productOptionNameLBL.text = option.name
        productOptionPriceLBL.text = "$\(Int(option.price))"
        
    }

}
