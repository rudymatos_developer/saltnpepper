//
//  ConfirmationProductCell.swift
//  Salt&Pepper
//
//  Created by Rudy E Matos on 6/29/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class ConfirmationProductTVC: UITableViewCell {
    
    @IBOutlet weak var productNameLBL: UILabel!
    @IBOutlet weak var productPriceLBL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureView(product: Product){
        productNameLBL.text = product.name
        productPriceLBL.text = "$\(Int(product.price))"
    }
    

}
