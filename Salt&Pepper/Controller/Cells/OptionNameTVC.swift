//
//  OptionNameCell.swift
//  Salt&Pepper
//
//  Created by Rudy E Matos on 6/26/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class OptionNameTVC: UITableViewCell {

    @IBOutlet weak var optionNameLBL: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configureView(optionName: String){
        optionNameLBL.text = optionName
    }
    
}
