//
//  ProductTVCTableViewCell.swift
//  Salt&Pepper
//
//  Created by Rudy E Matos on 6/14/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class ProductTVC: UITableViewCell {
    
    @IBOutlet weak var isNewIV: UIImageView!
    @IBOutlet weak var productIV: UIImageView!
    @IBOutlet weak var productNameLBL: UILabel!
    @IBOutlet weak var productDescriptionLBL: UILabel!
    @IBOutlet weak var productPriceLBL: UILabel!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var mainView: UIView!
    
    fileprivate let cache = NSCache<NSString, UIImage>()
    fileprivate let firebaseService = FirebaseService()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    fileprivate func createCard(shadowView: UIView, mainView: UIView){
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowRadius = 4
        shadowView.layer.shadowOpacity = 0.2
        shadowView.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        mainView.layer.cornerRadius = 10
    }
    
    func configureView(product: Product){
        
        createCard(shadowView: shadowView, mainView: mainView)
        
        isNewIV.alpha = product.isNew ? 1 : 0
        productIV.layer.cornerRadius = 18
        productIV.layer.masksToBounds = true
        productIV.image = #imageLiteral(resourceName: "salt-and-pepper")
        productNameLBL.text = product.name
        productDescriptionLBL.text = product.shortDescription ?? product.longDescription
        productPriceLBL.text = "$\(Int(product.price))"
        
        guard let thumbnail = product.thumbnail != nil ? product.thumbnail : product.images?.thumbnail else{
            return
        }
        
        if let image = cache.object(forKey: NSString(string: thumbnail)){
            self.productIV.image = image
        }else{
            self.firebaseService.download(category: product.category, image: thumbnail, completion: { (image) in
                if let image = image{
                    DispatchQueue.main.async {
                        self.productIV.image = image
                        self.cache.setObject(image, forKey: NSString(string: thumbnail))
                    }
                }
            })
            
        }
    }
}
