//
//  ProductDetailsVC.swift
//  Salt&Pepper
//
//  Created by Rudy E Matos on 6/26/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class ProductDetailsVC: UIViewController {
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var optionsViewHeight: NSLayoutConstraint!
    @IBOutlet weak var optionsTV: UITableView!
    @IBOutlet weak var mainViewHeight: NSLayoutConstraint!
    @IBOutlet weak var optionsView: UIView!
    @IBOutlet weak var orderNowView: UIView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var productNameLBL: UILabel!
    @IBOutlet weak var orderNowBTNView: UIView!
    @IBOutlet weak var priceView: UIView!
    
    private var isCurrentlyShowingProductOptions = false
    private var optionsTVContentHeight : CGFloat = 0
    
    @IBOutlet weak var productIV: UIImageView!
    @IBOutlet weak var productPriceLBL: UILabel!
    @IBOutlet weak var productTitleLBL: UILabel!
    @IBOutlet weak var productDescriptionLBL: UILabel!
    @IBOutlet weak var productPriceWithOptionsLBL: UILabel!
    @IBOutlet weak var nfCaloriesLBL: UILabel!
    @IBOutlet weak var nfFatLBL: UILabel!
    @IBOutlet weak var nfCarbsLBL: UILabel!
    @IBOutlet weak var nfProteinLBL: UILabel!
    
    @IBOutlet weak var additionalProductPriceWithOptionsView: UIView!
    @IBOutlet weak var additionalProductPriceWithOptionsLBL: UILabel!
    fileprivate var isShowingAdditionalProductPriceWithOptionsView = false
    fileprivate let feedbackNotification = UINotificationFeedbackGenerator()
    
    var viewModel : ProductDetailVM!
    fileprivate var timer: Timer?
    fileprivate var productImages = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        timer?.invalidate()
    }
    
    deinit {
        print("exiting")
    }
    
    private func configureView(){
        additionalProductPriceWithOptionsView.alpha = 0
        orderNowBTNView.layer.cornerRadius = 20
        backView.backgroundColor = UIColor.clear
        backView.layer.cornerRadius = 20
        productNameLBL.layer.shadowColor = UIColor.black.cgColor
        productNameLBL.layer.shadowOpacity = 0.5
        productNameLBL.layer.shadowOffset = CGSize.zero
        viewModel.downloadProductImages { [unowned self] (productImages) in
            if let productImages = productImages{
                self.productImages = productImages
                self.timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(ProductDetailsVC.animateProductImages), userInfo: nil, repeats: true)
                self.timer?.fire()
            }
        }
        
        viewModel.getAllOptionsForProduct { (options) in
            if let options = options {
                self.viewModel.setProductOptions(productOptions: options)
                DispatchQueue.main.async {
                    self.configureOptionsTable()
                }
            }
        }
        assignProductInfo()
        createRightRoundedLabel()
        configureAdditionalPriceWithOptionsView()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "confirmationSegue", let vc = segue.destination as? AddToCartConfirmationVC{
            vc.viewModel = AddToCartConfirmationVM(userService: viewModel.userService, orderProductWithOptions: viewModel.generateOrderProductWithOptions())
        }
    }
    
    @objc fileprivate func animateProductImages(){
        var tempProductImage = productImages
        if let currentImage = productIV.image, let index = productImages.firstIndex(of: currentImage){
            tempProductImage.remove(at: index)
        }
        let randomImage = tempProductImage.randomElement()
        productIV.alpha = 0
        productIV.image = randomImage
        UIView.animate(withDuration: 0.5) {
            self.productIV.alpha = 1
        }
    }
    
    
    //MARK: - UI Configurations
    fileprivate func configureAdditionalPriceWithOptionsView(){
        let path = UIBezierPath(roundedRect: additionalProductPriceWithOptionsView.bounds, byRoundingCorners: [.topLeft,.bottomLeft], cornerRadii: CGSize(width: 15, height: 15))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        mask.frame = additionalProductPriceWithOptionsView.bounds
        additionalProductPriceWithOptionsView.layer.mask = mask
    }
    
    fileprivate func assignProductInfo(){
        productPriceLBL.text = viewModel.getFormattedProductPrice()
        productTitleLBL.text = viewModel.selectedProduct.name
        productDescriptionLBL.text = viewModel.selectedProduct.longDescription
        additionalProductPriceWithOptionsLBL.text = viewModel.getFormattedTotalPriceWithOptions()
        productPriceWithOptionsLBL.text = viewModel.getFormattedTotalPriceWithOptions()
        nfCaloriesLBL.text = viewModel.getProductNutritionFact(type: .calories)
        nfFatLBL.text = viewModel.getProductNutritionFact(type: .fats)
        nfCarbsLBL.text = viewModel.getProductNutritionFact(type: .carbs)
        nfProteinLBL.text = viewModel.getProductNutritionFact(type: .protein)
    }
    
    fileprivate func createRightRoundedLabel() {
        let bezier = UIBezierPath(roundedRect: priceView.bounds, byRoundingCorners: [.topRight], cornerRadii: CGSize(width: 15, height: 15))
        let mask = CAShapeLayer()
        mask.path = bezier.cgPath
        mask.frame = priceView.bounds
        priceView.layer.mask = mask
    }
    
    fileprivate func configureOptionsTable(){
        optionsTV.estimatedRowHeight = 0
        optionsTV.reloadData()
        view.layoutIfNeeded()
        optionsTVContentHeight = optionsTV.contentSize.height
    }
    
    private func toggleProductOptions(){
        isCurrentlyShowingProductOptions = !isCurrentlyShowingProductOptions
        optionsView.isHidden = !isCurrentlyShowingProductOptions
        orderNowView.isHidden = !isCurrentlyShowingProductOptions
        let newHeight = optionsTVContentHeight + orderNowView.frame.height
        optionsViewHeight.constant = optionsTVContentHeight
        mainViewHeight.constant +=  isCurrentlyShowingProductOptions ? newHeight : -newHeight
    }
    
    //MARK: - UI Buttons Actions
    @IBAction func toggleFavoriteProduct(_ sender: UITapGestureRecognizer) {
        print("toogle Favorite")
    }
    
    @IBAction func toggleProductOptions(_ sender: UITapGestureRecognizer) {
        toggleProductOptions()
    }
    
    @IBAction func addItemToOrder(_ sender: UIButton) {
        performSegue(withIdentifier: "confirmationSegue", sender: nil)
    }
    
    @IBAction func goBAck(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: - Scroll Delegate Extension

extension ProductDetailsVC: UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y >= 250{
            UIView.animate(withDuration: 0.3) {
                self.backView.backgroundColor = UIColor(named: "MainColor")
            }
        }else{
            UIView.animate(withDuration: 0.3) {
                self.backView.backgroundColor = UIColor.clear
            }
        }
        
        if scrollView.contentOffset.y >= 550{
            if !isShowingAdditionalProductPriceWithOptionsView{
                additionalProductPriceWithOptionsView.transform = CGAffineTransform(translationX: 100, y: 0)
                additionalProductPriceWithOptionsView.alpha = 1
                UIView.animate(withDuration: 0.3) {
                    self.additionalProductPriceWithOptionsView.transform = CGAffineTransform.identity
                    self.configureAdditionalPriceWithOptionsView()
                }
                isShowingAdditionalProductPriceWithOptionsView = true
            }
        }else{
            if isShowingAdditionalProductPriceWithOptionsView{
                isShowingAdditionalProductPriceWithOptionsView = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.additionalProductPriceWithOptionsView.transform = CGAffineTransform(translationX: 100, y: 0)
                }) { (completed) in
                    self.additionalProductPriceWithOptionsView.alpha = 0
                    self.configureAdditionalPriceWithOptionsView()
                }
            }
        }
        
    }
}

extension ProductDetailsVC: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 50
        }else{
            return 80
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != 0{
            viewModel.selectOption(onSection: indexPath.section, onRow: indexPath.row - 1 )
            feedbackNotification.notificationOccurred(.warning)
            productPriceWithOptionsLBL.text = "$\(viewModel.getTotalPriceWithOptions())0"
            additionalProductPriceWithOptionsLBL.text = "$\(viewModel.getTotalPriceWithOptions())0"
            if isShowingAdditionalProductPriceWithOptionsView{
                additionalProductPriceWithOptionsView.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
                UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 0.2, options: [.curveEaseOut], animations: {
                    self.additionalProductPriceWithOptionsView.transform = CGAffineTransform.identity
                }, completion: nil)
            }else{
                productPriceWithOptionsLBL.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
                UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 0.2, options: [.curveEaseOut], animations: {
                    self.productPriceWithOptionsLBL.transform = CGAffineTransform.identity
                }, completion: nil)
            }
            
            DispatchQueue.main.async {
                self.optionsTV.reloadData()
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.getProductOptionsCount()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getNumberOrRowForSection(section: section) + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "optionNameCell", for: indexPath) as! OptionNameTVC
            let selectedText = viewModel.getProductOption(forSection: indexPath.section).title
            cell.configureView(optionName: selectedText)
            cell.selectionStyle = .none
            return cell
        }else{
            let cell =  tableView.dequeueReusableCell(withIdentifier: "optionItemCell", for: indexPath) as! OptionItemTVC
            let productOption = viewModel.getOption(onSection: indexPath.section, onRow: indexPath.row - 1)
            
            cell.configureView(productOption: productOption)
            cell.selectionStyle = .none
            return cell
        }
    }
}
