//
//  ViewController.swift
//  Salt&Pepper
//
//  Created by Rudy E Matos on 6/14/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class ProductListVC: UIViewController {
    
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var loginCredentials: UIStackView!
    @IBOutlet weak var searchViewWidthScreenConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchViewFixedWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var categoriesCV: UICollectionView!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var searchTFView: UIView!
    @IBOutlet weak var productsTV: UITableView!
    @IBOutlet weak var productTotalCountLBL: UILabel!
    
    fileprivate var viewModel = ProductListVM(firebaseService: FirebaseService(), userService: UserService())

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        viewModel.loadProducts {
            DispatchQueue.main.async { [weak self] in
                self?.productTotalCountLBL.text = self?.viewModel.getTotalProductText()
                self?.productsTV.reloadData()
                self?.categoriesCV.reloadData()
            }
        }
    }
    
    fileprivate func configureView(){
        self.tabBarController?.tabBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.isHidden = true
        applyMaskToSearchView()
        self.searchTFView.isHidden = !viewModel.isSearchExpanded
        searchTFView.layer.borderWidth = 0.2
        searchTFView.layer.borderColor = UIColor.white.cgColor
        searchTFView.layer.cornerRadius = 10
        searchTF.attributedPlaceholder = NSAttributedString(string: "Que se te antoja?", attributes: [NSAttributedStringKey.foregroundColor : UIColor.lightGray])
        self.view.layoutIfNeeded()
    }
    
    fileprivate func applyMaskToSearchView(){
        let path = UIBezierPath(roundedRect: searchView.bounds, byRoundingCorners: [.topLeft, .bottomLeft], cornerRadii: CGSize(width: 15, height: 15))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        mask.frame = searchView.bounds
        searchView.layer.mask = mask
    }
    
    @IBAction func searchProduct(_ sender: UITextField) {
        if let text = sender.text{
            searchProduct(withText: text)
        }
    }
    
    fileprivate func searchProduct(withText : String){
        viewModel.selectedCategory = SystemCategories.search.rawValue
        viewModel.searchParam = withText
        viewModel.cleanUpSearchResultAnimatedIndex()
        productTotalCountLBL.text = viewModel.getTotalProductText()
        DispatchQueue.main.async {
            self.productsTV.reloadData()
        }
    }
    
    fileprivate func toggleSearchView(){
        viewModel.isSearchExpanded.toggle()
        searchViewFixedWidthConstraint.priority = viewModel.isSearchExpanded ? UILayoutPriority(1) : UILayoutPriority(999)
        searchViewWidthScreenConstraint.priority = viewModel.isSearchExpanded ? UILayoutPriority(999) : UILayoutPriority(1)
        if viewModel.isSearchExpanded{
            searchTF.becomeFirstResponder()
            if let text = searchTF.text{
                searchProduct(withText: text)
            }
            categoriesCV.setContentOffset(CGPoint(x: categoriesCV.contentSize.width - categoriesCV.bounds.width + categoriesCV.contentInset.right, y: 0), animated: true)
        }else{
            view.endEditing(true)
        }
        UIView.animate(withDuration: 0.5, animations: {
            self.searchView.alpha = self.viewModel.isSearchExpanded ? 0.94 : 1
            self.searchTFView.isHidden = !self.viewModel.isSearchExpanded
            self.view.layoutIfNeeded()
            self.loginCredentials.alpha = self.viewModel.isSearchExpanded ? 0 : 1
            if self.viewModel.isSearchExpanded{
                self.applyMaskToSearchView()
            }
        }) { (completed) in
            if !self.viewModel.isSearchExpanded{
                self.applyMaskToSearchView()
            }
        }
    }
    
    @IBAction func comingBackFromProductConfirmation(segue : UIStoryboardSegue){
        if viewModel.userService.getOrderProductsCount() > 0 {
            self.tabBarController?.tabBar.items?[1].badgeValue = "\(viewModel.userService.getOrderProductsCount())"
        }
    }
    
    @IBAction func toggleSearchView(_ sender: UIButton) {
        toggleSearchView()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showProductDetailsSegue" , let vc = segue.destination as? ProductDetailsVC, let selectedProduct = viewModel.selectedProduct{
            vc.viewModel = ProductDetailVM(firebaseService: viewModel.firebaseService,userService: viewModel.userService, selectedProduct: selectedProduct)
        }
    }
}

extension ProductListVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 100, 0, 100)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.getCategoriesCount()
    }
    
    func getMidVisibleIndexPath() -> IndexPath? {
        var visibleRect = CGRect()
        visibleRect.origin = self.categoriesCV.contentOffset
        visibleRect.size = self.categoriesCV.bounds.size
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        guard let indexPath = self.categoriesCV.indexPathForItem(at: visiblePoint) else { return nil }
        return indexPath
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let indexPath = getMidVisibleIndexPath(){
            let selectedCategory = viewModel.getCategory(byIndex: indexPath.row)
            if selectedCategory != viewModel.selectedCategory{
                viewModel.selectedCategory = selectedCategory
                if selectedCategory == viewModel.getCategory(byIndex: viewModel.getCategoriesCount() - 1 ), !viewModel.isSearchExpanded{
                    toggleSearchView()
                }else{
                    if viewModel.isSearchExpanded {toggleSearchView()}
                    productTotalCountLBL.text = viewModel.getTotalProductText()
                    viewModel.searchParam = ""
                    DispatchQueue.main.async {
                        self.productsTV.reloadData()
                    }
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didUpdateFocusIn context: UICollectionViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if let nextFocusedIndex = context.nextFocusedIndexPath{
            categoriesCV.scrollToItem(at: nextFocusedIndex, at: .centeredHorizontally, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryCell", for: indexPath) as? CategoryCVC else{
            return UICollectionViewCell()
        }
        let categoryName = viewModel.getCategory(byIndex: indexPath.row)
        cell.configureView(text: categoryName)
        return cell
    }
    
}

extension ProductListVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !viewModel.wasAnimated(onIndex: indexPath.row){
            cell.alpha = 0
            viewModel.setIndexWasAnimated(index: indexPath.row)
            let delay = TimeInterval(Double(indexPath.row) / Double(4))
            cell.transform = CGAffineTransform(translationX: 0, y: 10)
            UIView.animate(withDuration: 0.2, delay: delay, options: [.curveEaseIn], animations: {
                cell.transform = CGAffineTransform.identity
                cell.alpha = 1
            }, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        viewModel.selectedProduct = viewModel.getProduct(byIndex: indexPath.row)
        performSegue(withIdentifier: "showProductDetailsSegue", sender: nil)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getProducts()?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "productCell", for: indexPath) as? ProductTVC, let selectedProduct =  viewModel.getProduct(byIndex: indexPath.row) else{
            return UITableViewCell()
        }
        cell.configureView(product: selectedProduct)
        return cell
    }
}

