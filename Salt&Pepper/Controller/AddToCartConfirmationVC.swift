//
//  AddToCartConfirmationVC.swift
//  Salt&Pepper
//
//  Created by Rudy E Matos on 6/28/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class AddToCartConfirmationVC: UIViewController {

    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var shadowViewHeight: NSLayoutConstraint!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var greetingLBL: UILabel!
    @IBOutlet weak var subtotalLBL: UILabel!
    @IBOutlet weak var optionsTotalLBL: UILabel!
    @IBOutlet weak var totalLBL: UILabel!
    @IBOutlet weak var productConfirmationTV: UITableView!
    
    private let notification = UINotificationFeedbackGenerator()
    var viewModel : AddToCartConfirmationVM!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        notification.notificationOccurred(.success)
        configureView()
    }
    
    @IBAction func confirm(_ sender: UIButton) {
        viewModel.addProductToOrder()
        performSegue(withIdentifier: "goBackToProductListSegue", sender: nil)
    }
    
    fileprivate func createCardView() {
        shadowView.layer.masksToBounds = false
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOffset = CGSize.zero
        shadowView.layer.shadowRadius = 4
        shadowView.layer.shadowOpacity = 0.5
        mainView.layer.masksToBounds = true
        mainView.layer.cornerRadius = 10
    }
    
    private func configureView(){
        shadowViewHeight.constant = CGFloat(viewModel.getViewDimension())
        view.setNeedsLayout()
        greetingLBL.text = "Hola \(viewModel.getUserDisplayName())"
        subtotalLBL.text = "$\(viewModel.getProductPrice())"
        optionsTotalLBL.text = "$\(viewModel.getOptionsTotalPrice())"
        totalLBL.text = "$\(viewModel.getProductWithOptionsTotal())"
        createCardView()
    }
}

extension AddToCartConfirmationVC: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.getSectionCount()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1 : viewModel.getProductOptionsCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "confirmationProductCell", for: indexPath) as! ConfirmationProductTVC
            cell.configureView(product: viewModel.getSelectedProduct())
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "confirmationProductOptionCell", for: indexPath) as! ConfirmationProductOptionTVC
            cell.configureView(option: viewModel.getOption(onRow: indexPath.row))
            return cell
        }
    }
    
    
}
