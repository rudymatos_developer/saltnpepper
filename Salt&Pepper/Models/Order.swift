//
//  Order.swift
//  Salt&Pepper
//
//  Created by Rudy E Matos on 6/28/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation

struct OrderProductWithOptions: Codable{
    var product: Product
    var selectedOptions : [ProductOption.Option]
    
    init(product: Product, selectedOptions: [ProductOption.Option]){
        self.product = product
        self.selectedOptions = selectedOptions
    }
    
}

struct Order: Codable, FirebaseType{
    
    static var collectionName: String = "orders"
    
    init(userId: String){
        self.orderId = UUID().uuidString
        self.userId = userId
        self.status = .pending
        self.orderTotal = 0
        self.taxes = 0
        self.products = [OrderProductWithOptions]()
    }
    
    init?(data: [String : Any]) {
        do{
            let jsonObjectData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
            let object = try JSONDecoder().decode(Order.self, from: jsonObjectData)
            self = object
        }catch{
            return nil
        }
    }
    
    mutating func calculateOrderTotal(){
        orderTotal = products.reduce(0) {currentTotal, currentProduct in
            return currentTotal + currentProduct.product.price + currentProduct.selectedOptions.reduce(0) {totalOptionsTotal, currentOption in
                return totalOptionsTotal + currentOption.price
            }
        }
    }
    
    mutating func parse() -> [String: Any]?{
        guard let data = try? JSONEncoder().encode(self), let dictionary =  try? JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String:Any] else{
            return nil
        }
        return dictionary
    }
    
    var orderId: String
    var placedOn: Date?
    var userId: String
    var orderTotal : Double
    var taxes: Double
    var status : OrderStatus
    var products : [OrderProductWithOptions]

    enum OrderStatus: String, Codable{
        case pending = "pending"
        case completed = "completed"
        case cancelled = "cancelled"
    }

    enum CodingKeys: String, CodingKey{
        case orderId = "order_id"
        case placedOn = "order_placed_on_date"
        case userId = "user_id"
        case status
        case orderTotal = "order_total"
        case taxes
        case products
    }
    
}
