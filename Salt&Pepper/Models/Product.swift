//
//  Product.swift
//  Salt&Pepper
//
//  Created by Rudy E Matos on 6/14/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit
import Firebase

protocol FirebaseType{
    static var collectionName : String {get}
    init?(data: [String:Any])
}

struct ProductOption: Codable, FirebaseType{
    
    private(set) static var collectionName = "product_options"
    
    var options : [Option]
    var title: String
    
    struct Option: Codable{
        var isDefault: Bool
        var isSelected: Bool = false
        var description : String
        var name : String
        var index : Int
        var price : Double
        enum CodingKeys : String, CodingKey{
            case isDefault = "default"
            case description
            case index
            case name
            case price
        }
        
        func parse() -> [String:Any]{
            return ["name": name, "description":description,"price":price]
        }
        
    }
    
    init?(data: [String:Any]){
        do{
            let jsonData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
            let productOptions = try JSONDecoder().decode(ProductOption.self, from: jsonData)
            self = productOptions
            if let defaultElementIndex = options.firstIndex(where: {$0.isDefault}){
                options[defaultElementIndex].isSelected = true
            }
        }catch{
            return nil
        }
    }
    
}

struct Product: Codable,FirebaseType{
    
    private(set) static var collectionName = "products"
    
    var name : String
    var productId: String?
    var category : String
    var price : Double
    
    var images: Images?
    
    struct Images: Codable{
        var details : [String]
        var thumbnail : String
    }
    
    var nutritionFacts : NutritionFacts?
    
    struct NutritionFacts: Codable{
        var calories: Int
        var carbs : Double
        var fat: Double
        var protein: Double
    }
    
    var thumbnail: String?
    var longDescription : String
    var shortDescription : String?
    var rating: Double
    var isNew : Bool
    var isOffer : Bool
    
    enum CodingKeys: String, CodingKey{
        case name
        case productId = "product_id"
        case category
        case price
        case images
        case thumbnail
        case rating
        case isNew = "is_new"
        case isOffer = "is_offer"
        case nutritionFacts = "nutritions_facts"
        case longDescription = "long_description"
        case shortDescription = "short_description"
    }
    
    init?(data: [String:Any]){
        do{
            let jsonDataObject = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
            let productObject = try JSONDecoder().decode(Product.self, from: jsonDataObject)
            self = productObject
        }catch{
            return nil
        }
    }
    
}




