//
//  FirebaseDataService.swift
//  Salt&Pepper
//
//  Created by Rudy E Matos on 6/14/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation
import Firebase

class FirebaseService{
    
    fileprivate let db = Firestore.firestore()
    fileprivate let storage = Storage.storage()
    
    init(){
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        db.settings = settings
    }
    
    func downloadProductImages(product : Product , completion : @escaping ([UIImage]?) -> Void){
        guard let details = product.images?.details, details.count > 0 else{
            completion(nil)
            return
        }
        var totalDownloads = 0
        var images = [UIImage]()
        for currentImageName in details{
            download(category: product.category, image: currentImageName) { (image) in
                totalDownloads += 1
                if let image = image{
                    images.append(image)
                }
                if totalDownloads == details.count{
                    completion(images)
                }
            }
        }
    }
    
//    func addProductToOrder(product: MenuProduct, withProductOptions: [ProductOptions.Option]){
    
//    }
    
  
    func download(category: String, image: String, completion : @escaping (UIImage?) -> Void){
        let storageRef = storage.reference().child("\(category)/\(image).png")
        storageRef.getData(maxSize: 1*1024*1024) { (data, error) in
            guard let data = data, let thumbnail = UIImage(data: data), error == nil else{
                print(error)
                completion(nil)
                return
            }
            completion(thumbnail)
        }
    }
    
    func getAll<T: FirebaseType>(classType: T.Type, withField: String, andValue: Any, completion : @escaping ([T]?) -> Void){
        db.collection(classType.collectionName).whereField(withField, isEqualTo: andValue).getDocuments { (snapshot, error) in
            guard let snapshot = snapshot, snapshot.documents.count > 0,  error == nil else{
                completion(nil)
                return
            }
            
            let results = snapshot.documents.compactMap({T(data:$0.data())})
            completion(results)
        }
    }
    
    
    func getAll<T: FirebaseType>(classType: T.Type,completion : @escaping ([T]?) -> Void){
        db.collection(classType.collectionName).getDocuments { (snapshot, error) in
            guard let snapshot = snapshot, snapshot.documents.count > 0,  error == nil else{
                completion(nil)
                return
            }
            
            let results = snapshot.documents.compactMap({T(data:$0.data())})
            completion(results)
        }
    }
}

