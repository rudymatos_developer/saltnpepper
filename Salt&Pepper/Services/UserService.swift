//
//  UserService.swift
//  Salt&Pepper
//
//  Created by Rudy E Matos on 6/28/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation

struct User{
    var username = "juanosvaldoc"
    var displayName = "Juan Cordero"
}

class UserService{

    var currentOrder : Order!
    private var user = User()
    
    func getCurrentUser() -> User{
        return user
    }
    
    init(){
        initOrder()
    }
    
    func getOrderProductsCount() -> Int{
        return currentOrder.products.count
    }

    func initOrder(){
        currentOrder = Order(userId: user.username)
    }
    
    
}
