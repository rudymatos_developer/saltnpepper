//
//  ProductListVM.swift
//  Salt&Pepper
//
//  Created by Rudy E Matos on 6/15/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation

protocol ProductListType{
    var firebaseService : FirebaseService {get}
    var userService : UserService {get}
}

enum SystemCategories: String{
    case isNew = "Nuevos"
    case offers = "Ofertas"
    case search = "Buscar"
}


class ProductListVM: ProductListType{
    
    private(set) var firebaseService: FirebaseService
    private(set) var userService: UserService
    
    private var filteredProductList = [Product]()
    private var originalProductList = [Product]()
    private var animatedProductList = [String: [Int]]()
    private var categories = [String]()
    
    var isSearchExpanded = false
    var selectedCategory = ""
    var selectedProduct: Product?
    var searchParam = ""
    
    init(firebaseService : FirebaseService, userService : UserService){
        self.firebaseService = firebaseService
        self.userService = userService
    }
    
    
    func loadProducts(completion: @escaping () -> Void){
        firebaseService.getAll(classType: Product.self) { [weak self] (products) in
            if let products = products{
                self?.originalProductList = products
                self?.loadCategories()
                completion()
            }
        }
    }
    
    private func loadCategories(){
        categories = Array(Set(originalProductList.compactMap({$0.category.capitalized}))).sorted()
        categories.append(SystemCategories.isNew.rawValue)
        categories.append(SystemCategories.offers.rawValue)
        categories.append(SystemCategories.search.rawValue)
        selectedCategory = categories[0]
    }
    
    func getCategoriesCount() -> Int{
        return categories.count
    }
    
    func getCategory(byIndex: Int) -> String{
        return categories[byIndex]
    }
    
    func setIndexWasAnimated(index: Int){
        if animatedProductList[selectedCategory] == nil{
            animatedProductList[selectedCategory] = [Int]()
        }
        animatedProductList[selectedCategory]?.append(index)
    }
    
    func cleanUpSearchResultAnimatedIndex(){
        animatedProductList[SystemCategories.search.rawValue] = [Int]()
    }
    
    func wasAnimated(onIndex: Int) -> Bool{
        guard let categoryIndexes = animatedProductList[selectedCategory], categoryIndexes.contains(onIndex) else{
            return false
        }
        return true
    }
    
    func getProducts() -> [Product]?{
        if let systemCategory = SystemCategories(rawValue: selectedCategory){
            switch systemCategory{
            case .isNew:
                return originalProductList.filter({$0.isNew})
            case .offers:
                return originalProductList.filter({$0.isOffer})
            case .search:
                return originalProductList.filter({$0.name.uppercased().contains(searchParam.uppercased())})
            }
        }else{
            return originalProductList.filter({$0.category.uppercased().contains(selectedCategory.uppercased())})
        }
    }
    
    func getProduct(byIndex: Int) -> Product?{
        return getProducts()?[byIndex]
    }
    
    func getTotalProductText() -> String{
        let productTotal = getProducts()?.count ?? 0
        if productTotal > 0{
            return "\(productTotal) \(productTotal == 1 ? "plato" : "platos") en total"
        }else{
            return "No existe ningun plato"
        }
    }
    
}
