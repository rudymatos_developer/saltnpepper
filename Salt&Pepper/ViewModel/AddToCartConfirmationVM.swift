//
//  AddToCartConfirmationVM.swift
//  Salt&Pepper
//
//  Created by Rudy E Matos on 6/29/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation

struct AddToCartConfirmationVM{
    
    private(set) var userService : UserService
    private(set) var orderProductWithOptions: OrderProductWithOptions
    
    init(userService: UserService, orderProductWithOptions: OrderProductWithOptions){
        self.userService = userService
        self.orderProductWithOptions = orderProductWithOptions
    }
    
    func getOption(onRow: Int) -> ProductOption.Option{
        return orderProductWithOptions.selectedOptions[onRow]
    }
    
    func getSelectedProduct() -> Product{
        return orderProductWithOptions.product
    }
    
    func getViewDimension() -> Int{
        switch orderProductWithOptions.selectedOptions.count{
        case 0:
            return 345
        case 1:
            return 390
        default:
            return 440
        }
    }
    
    func getUserDisplayName() -> String{
        return userService.getCurrentUser().displayName
    }
    
    func addProductToOrder(){
        userService.currentOrder.products.append(orderProductWithOptions)
        userService.currentOrder.calculateOrderTotal()
    }
    
    func getProductWithOptionsTotal() -> Int{
        return getProductPrice() + getOptionsTotalPrice()
    }
    
    func getProductPrice() -> Int{
        return Int(orderProductWithOptions.product.price)
    }
    
    func getOptionsTotalPrice() -> Int{
        return Int(orderProductWithOptions.selectedOptions.reduce(0){$0 + $1.price})
    }
    
    func getProductOptionsCount() -> Int{
        return orderProductWithOptions.selectedOptions.count
    }
    
    func getSectionCount() -> Int{
        return 2
    }
    
}
