//
//  ProductDetailVM.swift
//  Salt&Pepper
//
//  Created by Rudy E Matos on 6/27/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit

struct ProductDetailVM{
    
    private(set) var firebaseService : FirebaseService
    private(set) var userService : UserService
    
    var selectedProduct : Product
    fileprivate var productOptions = [ProductOption]()
    fileprivate var selectedOptions = [Int:Int]()
    
    init(firebaseService : FirebaseService, userService: UserService, selectedProduct: Product){
        self.firebaseService = firebaseService
        self.selectedProduct = selectedProduct
        self.userService = userService
    }
    
    func getFormattedTotalPriceWithOptions() -> String{
        return "$\(getTotalPriceWithOptions())0"
    }
    
    func getFormattedProductPrice() -> String{
        return "RD$ \(selectedProduct.price)0"
    }
    
    func getProductOption(forSection: Int) -> ProductOption{
        return productOptions[forSection]
    }
    
    func getOption(onSection: Int, onRow: Int) -> ProductOption.Option{
        return productOptions[onSection].options.sorted(by: {$0.index < $1.index})[onRow]
    }
    
    func generateOrderProductWithOptions() -> OrderProductWithOptions{
        return OrderProductWithOptions(product: selectedProduct, selectedOptions: getSelectedOptions())
    }
    
    mutating func selectOption(onSection: Int, onRow: Int){
        if let previousSelectedRowOnSection = selectedOptions[onSection], previousSelectedRowOnSection != onRow{
            productOptions[onSection].options[previousSelectedRowOnSection].isSelected = false
        }
        productOptions[onSection].options[onRow].isSelected = true
        selectedOptions[onSection] = onRow
    }
    
    
    func getTotalPriceWithOptions() -> Double{
        let totalInOptions : Double = getSelectedOptions().reduce(0) {$0 + $1.price}
        return selectedProduct.price + totalInOptions
    }

    func getSelectedOptions() -> [ProductOption.Option]{
        var selectedOptionsList = [ProductOption.Option]()
        selectedOptions.forEach({selectedOptionsList.append(productOptions[$0.key].options[$0.value])})
        return selectedOptionsList
    }
    
    func getProductOptionsCount() -> Int{
        return productOptions.count
    }
    
    func getNumberOrRowForSection(section: Int) -> Int{
        return productOptions[section].options.count
    }
    
    func downloadProductImages(completion: @escaping ([UIImage]?) -> Void){
        firebaseService.downloadProductImages(product: selectedProduct) { (images) in
            completion(images)
        }
    }
    
    mutating func setProductOptions(productOptions: [ProductOption]){
        self.productOptions = productOptions
        for index in 0..<productOptions.count{
            if let optionIndex = productOptions[index].options.firstIndex(where: {$0.isSelected}){
                selectedOptions[index] = optionIndex
            }
        }
    }
    
    func getAllOptionsForProduct(completion: @escaping ([ProductOption]?) -> Void){
        if let productId = selectedProduct.productId{
            firebaseService.getAll(classType: ProductOption.self, withField: "products_list.\(productId)", andValue: true) { (options) in
                guard let options = options else{
                    completion(nil)
                    return
                }
                completion(options)
            }
        }else{
            completion(nil)
        }
    }
    
    enum NutritionFactType{
        case carbs
        case calories
        case fats
        case protein
    }
    
    func getProductNutritionFact(type: NutritionFactType) -> String{
        guard let nutritionFacts = selectedProduct.nutritionFacts else{
            return "0g"
        }
        switch type {
        case .calories:
            return "\(nutritionFacts.calories)g"
        case .carbs:
            return "\(nutritionFacts.carbs)g"
        case .fats:
            return "\(nutritionFacts.fat)g"
        case .protein:
            return "\(nutritionFacts.protein)g"
        }
    }
}
