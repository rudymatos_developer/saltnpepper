# SaltNPepper iPhone App

Este proyecto contiene los archivos necesarios para descargar e instalar la aplicacion de SaltNPepper for iPhone.
La aplicacion se encuentra actualmente en estado de **desarrollo** y su implementacion pudiera variar dependiendo de la existencia
de nuevos requerimientos. 

Esta aplicacion es parte del Ecosistema de SaltNPepper creado para la implemetacion de un sistema de pedido de comida dentro de un
rango establecido para uso de Restaurantes, Food Trucks y sitios de ventas de Comida.

## Ecosistema

El mismo consiste en 3 partes esenciales

1. Aplicacion SaltNPepper para iPhones desde donde los usuarios pueden realizar sus pedidos
2. Aplicacion SaltNPepper Kitchen para iPads donde los restaurantes pueden ver los pedidos realizados por los usuarios, confirmar los pedidos y
cambiar los estatus de los mismos
3. Aplicacion SaltNPepper Admin/Report Website donde los administradores pueden ver detalles de ventas y administrar productos 

## Desarrollo
La mayor parte del desarrollo fue realizado utilizando Swift, Vapor , TypeScript y Firebase
